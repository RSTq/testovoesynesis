package by.rstq.syn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;


import static java.lang.Integer.parseInt;

@RestController
@RequestMapping("producer")
public class Controller {

    Logger logger = LoggerFactory.getLogger(Controller.class);

    @Value("${consumer.ip}")
    String consumer;

    @PostMapping
    public void call(@RequestParam("callTimes") String var) {
        RestTemplate restTemplate = new RestTemplate();
        int k = 0;
        try {
            k = parseInt(var);
            for (int i = 0; i < k; i++) {
            restTemplate.postForLocation(consumer, null);
            }
        }
        catch(Exception e) {
            logger.error("wrong callTimes, integer needed or Consumer server is not working");
        }
    }
}
